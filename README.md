# This project is a sample skeleton for Express framework


## Installation

- Clone the project
- Rename .env.example to .env and update the database connection info
- Init

```bash
npm install
node_modules/.bin/sequelize db:migrate
node_modules/.bin/sequelize db:seed:all
```
- Run

```bash
npm run devstart
```
- Sample API:
[http://localhost:3000/users?page=1](http://localhost:3000/users?page=1)

## Dependencies

- [dotenv](https://github.com/motdotla/dotenv) 
- [mysql2](https://github.com/brianmario/mysql2)
- [sequelize](https://github.com/sequelize/sequelize)
- [sequelize-cli](https://github.com/sequelize/cli)
- [nodemon](https://github.com/remy/nodemon)
