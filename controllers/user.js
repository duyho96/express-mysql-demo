var UserService = require('../services/user');

module.exports.getList = function (req, res) {
    var page = req.query.page || 1;
    return UserService.getList(page)
        .then(function (users) {
            res.json(users);
        })
        .catch(function (error) {
            res.json({message: error})
        });
};

module.exports.get = function (req, res) {
    res.json({message: 'TODO: implement get user detail API'});
};

module.exports.create = function (req, res) {
    res.json({message: 'TODO: implement create user API'});
};

module.exports.update = function (req, res) {
    res.json({message: 'TODO: implement update user API'});
};

module.exports.delete = function (req, res) {
    res.json({message: 'TODO: implement delete user API'});
};
