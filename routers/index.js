var express = require('express');
var router = express.Router();
var userRouter = require('./user');

router.use('/users', userRouter);

module.exports = router;
