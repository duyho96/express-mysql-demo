var express = require('express');
var router = express.Router();
var UserController = require('../controllers/user');

router.get('/', UserController.getList);
router.get('/:id', UserController.get);
router.post('/', UserController.create);
router.put('/', UserController.update);
router.delete('/', UserController.delete);

module.exports = router;
