'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
              */

        var now = new Date();
        return queryInterface.bulkInsert('users', [
            {
                name: 'John Doe',
                username: 'johndoe',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Caroline Herschel',
                username: 'carolineherschel',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Dorothy Hodgkin',
                username: 'dorothyhodgkin',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Edmond Halley',
                username: 'edmondhalley',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Enrico Fermi',
                username: 'enricofermi',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Geraldine Seydoux',
                username: 'geraldineseydoux',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Jane Goodall',
                username: 'janegoodall',
                password: 'password',
                createdAt: now,
                updatedAt: now
            },
            {
                name: 'Lord Kelvin',
                username: 'lordkelvin',
                password: 'password',
                createdAt: now,
                updatedAt: now
            }

        ], {});
    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
                  */

        return queryInterface.bulkDelete('users', null, {});
    }
};
